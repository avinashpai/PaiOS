#ifndef UTILS_H
#define UTILS_H

#include <stddef.h>

void *memcpy (void *restrict dstptr, const void *restrict srcptr, size_t size);

#endif
