; GDT
gdt_start:

gdt_null:       ; null descriptor
  dd 0x0        ; dd = double word (4 bytes)
  dd 0x0        ; 8 bytes of 0's

gdt_code:       ; code segment descriptor
  dw 0xffff     ; limit (0-15)
  dw 0x0        ; base (0-15)
  db 0x0        ; base (16-23)
  db 10011010b  ; 1st flags, type falgs
  db 11001111b  ; 2nd flags, limit (16-19)
  db 0x0        ; base (24-31)


gdt_data:       ; data segment descriptor (same as code segment except for type flags)
  dw 0xffff
  dw 0x0
  db 0x0
  db 10010010b
  db 11001111b
  db 0x0

gdt_end:        ; end label so we can calcualte size of GDT for GDT descriptor

gdt_descriptor:
  dw gdt_end - gdt_start - 1    ; size of gdt, always one less
  dd gdt_start                  ; start address of GDT

CODE_SEG equ gdt_code - gdt_start
DATA_SEG equ gdt_data - gdt_start
