disk_load:
  push dx

  mov ah, 0x02 ; "read"
  mov al, dh   ; number of sectors to read
  mov ch, 0x00 ; cylinder
  mov dh, 0x00 ; head number
  mov cl, 0x02 ; first available sector after boot sector

  int 0x13      ; BIOS interrupt jump on error

  jc disk_error

  pop dx
  cmp dh, al
  jne disk_error
  ret

disk_error:
  mov bx, DISK_ERROR_MSG
  call print_string
  jmp $

DISK_ERROR_MSG db "Disk read error!", 0
