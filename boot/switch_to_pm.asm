[bits 16]
; Switch to protected mode
switch_to_pm:
  cli                   ; disable interrupts

.seta20_1:
  in byte al, 0x64
  test byte al, 0x2
  jnz .seta20_1

  mov byte al, 0xd1
  out byte 0x64, al

.seta20_2:
  in byte al, 0x64
  test byte al, 0x2
  jnz .seta20_2

  mov byte al, 0xdf
  out byte 0x60, al

  lgdt [gdt_descriptor] ; load gdt
  mov eax, cr0          ; to switch to pm we set the first bit
  or eax, 0x1           ; of CR0
  mov cr0, eax

  jmp CODE_SEG:init_pm  ; far jump to 32-bit code

[bits 32]
; Initialize registers and stack
init_pm:
  mov ax, DATA_SEG      ; in pm, old segments don't matter
  mov ds, ax            ; so point segment regs to data selector in GDT
  mov ss, ax
  mov es, ax
  mov fs, ax
  mov gs, ax

  mov ebp, 0x90000      ; update stack to top of the free space
  mov esp, ebp

  call BEGIN_PM

