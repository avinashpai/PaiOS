[bits 32]

VIDEO_MEMORY equ 0xb8000
WHITE_ON_BLACK equ 0x0f

print_string_pm:
  pusha
  mov edx, VIDEO_MEMORY ; set edx to beginning of vid memory

print_string_pm_loop:
  mov al, [ebx]         ; store char in al
  mov ah, WHITE_ON_BLACK ; store color in ax

  cmp al, 0     ; break on null terminator
  je print_string_pm_done

  mov [edx], ax         ; store ax in vid mem cell

  add ebx, 1            ; increment to next char
  add edx, 2            ; move to next cell

  jmp print_string_pm_loop

print_string_pm_done:
  popa
  ret
