print_string:
  pusha
  mov ah, 0x0e ; tele-prompt

  .run:
    mov al, [bx] ; load char fom bx
    inc bx
    cmp al, 0
    je .done
    int 0x10
    jmp .run

  .done:
    mov al, 0x0a ; newline
    int 0x10
    mov al, 0x0d ; carriage return
    int 0x10

    popa
    ret

