#ifndef SYSTEM_H
#define SYSTEM_H

#include <stddef.h>
#include <stdint.h>

uint8_t inb (uint16_t port);
void outb (uint16_t port, uint8_t data);
uint16_t inw (uint16_t port);
void outw (uint16_t port, uint16_t data);
uint32_t inl (uint32_t port);
void outl (uint32_t port, uint32_t data);

#endif
