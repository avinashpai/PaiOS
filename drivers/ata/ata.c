#include "../port.h"
#include "ata.h"

#define STATUS_BSY 0x80
#define STATUS_RDY 0x40
#define STATUS_DRQ 0x08
#define STATUS_DF 0x20
#define STATUS_ERR 0x01

static void ata_wait_bsy (void);
static void ata_wait_drq (void);

void
atapio_readsect (uint32_t target_addr, uint32_t lba, uint8_t count,
                 uint32_t *target)
{
  ata_wait_bsy ();
  outb (0x1F6, 0xE0 | ((lba >> 24) & 0xF));
  outb (0x1F2, count);
  outb (0x1F3, (uint8_t)lba);
  outb (0x1F4, (uint8_t) (lba >> 8));
  outb (0x1F5, (uint8_t) (lba >> 16));
  outb (0x1F7, 0x20);

  for (int i = 0; i < count; i++)
    {
      ata_wait_bsy ();
      ata_wait_drq ();
      for (int i = 0; i < 256; i++)
        target[i] = inw (0x1F0);
      target += 256;
    }
}

void
atapio_writesect (uint32_t lba, uint8_t count, uint32_t *bytes)
{
  ata_wait_bsy ();
  outb (0x1F6, 0xE0 | ((lba >> 24) & 0xF));
  outb (0x1F2, count);
  outb (0x1F3, (uint8_t)lba);
  outb (0x1F4, (uint8_t) (lba >> 8));
  outb (0x1F5, (uint8_t) (lba >> 16));
  outb (0x1F7, 0x30);

  for (int i = 0; i < count; i++)
    {
      ata_wait_bsy ();
      ata_wait_drq ();
      for (int i = 0; i < 256; i++)
        {
          outl (0x1F0, bytes[i]);
        }
    }
}

static void
ata_wait_bsy (void)
{
  while (inb (0x1F7) & STATUS_BSY)
    ;
}

static void
ata_wait_drq (void)
{
  while (!(inb (0x1F7) & STATUS_RDY))
    ;
}
