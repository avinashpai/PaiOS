#include <stdint.h>

void atapio_readsect (uint32_t target_addr, uint32_t lba, uint8_t count,
                      uint32_t *target);
void atapio_writesect (uint32_t lba, uint8_t count, uint32_t *bytes);
