#include "port.h"

uint8_t
inb (uint16_t port)
{
  uint8_t result;
  __asm__ __volatile__("in %%dx, %%al" : "=a"(result) : "d"(port));
  return result;
}

void
outb (uint16_t port, uint8_t data)
{
  __asm__ __volatile__("out %%al, %%dx" : : "a"(data), "d"(port));
}

uint16_t
inw (uint16_t port)
{
  uint16_t result;
  __asm__ __volatile__("in %%dx, %%ax" : "=a"(result) : "d"(port));
  return result;
}

void
outw (uint16_t port, uint16_t data)
{
  __asm__ __volatile__("out %%ax, %%dx" : : "a"(data), "d"(port));
}

uint32_t
inl (uint32_t port)
{
  uint32_t result;
  __asm__ __volatile__("inl %%dx,%%eax" : "=a"(result) : "d"(port));
  return result;
}

void
outl (uint32_t port, uint32_t value)
{
  __asm__ __volatile__("outl %%eax,%%dx" ::"d"(port), "a"(value));
}
