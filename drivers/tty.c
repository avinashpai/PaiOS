#include "../utils/utils.h"
#include "port.h"
#include "tty.h"
#include "vga.h"

static uint16_t *const VGA_MEMORY = (uint16_t *)0xb8000;
static const size_t VGA_COLS = 80;
static const size_t VGA_ROWS = 25;
static const int REG_SCREEN_CTRL = 0x3D4;
static const int REG_SCREEN_DATA = 0x3D5;

static size_t term_row;
static size_t term_col;
static uint8_t term_color_norm;
static uint8_t term_color_error;
static uint16_t *term_buffer;

void enable_cursor (uint8_t cursor_start, uint8_t cursor_end);
void disable_cursor (void);
void update_cursor (int x, int y);
uint16_t get_cursor_position (void);

// Calculate offset in term_buffer
static uint16_t
get_offset (size_t x, size_t y)
{
  return y * VGA_COLS + x;
}

// Clear screen
void
tty_clear (void)
{
  for (size_t col = 0; col < VGA_COLS; col++)
    {
      for (size_t row = 0; row < VGA_ROWS; row++)
        {
          const size_t index = get_offset (col, row);
          term_buffer[index] = vga_entry (' ', term_color_norm);
        }
    }

  update_cursor (0, 0);
}

// Initialize globals
// Clear screen
// Print welcome prompt
void
tty_init (void)
{
  term_row = 0;
  term_col = 0;
  term_color_norm = vga_entry_color (VGA_COLOR_LIGHT_BLUE, VGA_COLOR_WHITE);
  term_color_error = vga_entry_color (VGA_COLOR_RED, VGA_COLOR_WHITE);
  term_buffer = VGA_MEMORY;

  tty_clear ();

  tty_print ("PaiOS [v0.0.1]\n");
  tty_print (">>> Welcome to the kernel.\n\n");

  enable_cursor (0, 15);
}

// Put a character on the screen and move the cursor
void
tty_putc (char c)
{
  switch (c)
    {
    case '\n':
      {
        term_col = 0;
        term_row++;
        break;
      }
    case '\t':
      {
        term_col += 4;
        break;
      }
    default:
      {
        const size_t index = get_offset (term_col, term_row);
        term_buffer[index] = vga_entry ((unsigned char)c, term_color_norm);
        term_col++;
        break;
      }
    }

  if (term_col >= VGA_COLS)
    {
      term_col = 0;
      term_row++;
    }

  if (term_row >= VGA_ROWS)
    {
      for (size_t i = 1; i < VGA_ROWS; i++)
        {
          memcpy ((char *)(term_buffer + get_offset (0, i - 1)),
                  (char *)(term_buffer + get_offset (0, i)), VGA_COLS * 2);
        }

      for (size_t i = 0; i < VGA_COLS; i++)
        {
          term_buffer[get_offset (i, VGA_ROWS - 1)]
              = vga_entry (' ', term_color_norm);
        }

      term_row--;
    }

  update_cursor (term_col, term_row);
}

// Print strings
void
tty_print (const char *str)
{
  for (size_t i = 0; str[i] != '\0'; i++)
    tty_putc (str[i]);
}

// Enable cursor (0-15)
void
enable_cursor (uint8_t cursor_start, uint8_t cursor_end)
{
  outb (REG_SCREEN_CTRL, 0x0A);
  outb (REG_SCREEN_DATA, (inb (REG_SCREEN_DATA) & 0xC0) | cursor_start);

  outb (REG_SCREEN_CTRL, 0x0B);
  outb (REG_SCREEN_DATA, (inb (REG_SCREEN_DATA) & 0xE0) | cursor_end);
}

void
disable_cursor (void)
{
  outb (REG_SCREEN_CTRL, 0x0A);
  outb (REG_SCREEN_DATA, 0x20);
}

// Update cursor position
void
update_cursor (int x, int y)
{
  uint16_t pos = y * VGA_COLS + x;

  outb (REG_SCREEN_CTRL, 0x0F);
  outb (REG_SCREEN_DATA, (uint8_t) (pos & 0xFF));
  outb (REG_SCREEN_CTRL, 0x0E);
  outb (REG_SCREEN_DATA, (uint8_t) ((pos >> 8) & 0xFF));
}

// Return cursor position offset
uint16_t
get_cursor_position (void)
{
  uint16_t pos = 0;
  outb (REG_SCREEN_CTRL, 0x0F);
  pos |= inb (REG_SCREEN_DATA);
  outb (REG_SCREEN_CTRL, 0x0E);
  pos |= ((uint16_t)inb (REG_SCREEN_DATA)) << 8;
  return pos;
}
