#ifndef TTY_H
#define TTY_H

#include <stddef.h>

void tty_clear (void);
void tty_init (void);
void tty_putc (char c);
void tty_print (const char *str);
void printf (const char *format, ...);

#endif
