#include "../drivers/ata/ata.h"
#include "../drivers/tty.h"
#include "../utils/utils.h"

#if defined(__APPLE__)
#error "This code must be compiled with a cross-compiler"
#elif !defined(__i386__)
#error "This code must be compiled with an x86-elf compiler"
#endif

void
kmain (void)
{
  tty_init ();
  tty_print ("Work in progress...\n");
}
