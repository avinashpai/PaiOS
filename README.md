# PaiOS

## Introduction
Hobby x86 OS
  * Bootloader from scratch, no GRUB
  * 32-bit protected mode
  * Credit: Dr. Nick Blundell's unfinished [tutorial](https://www.cs.bham.ac.uk/~exr/lectures/opsys/10_11/lectures/os-dev.pdf)
## Dependencies
  * [GCC cross-compiler](https://wiki.osdev.org/GCC_Cross-Compiler)
  * [nasm](https://www.nasm.us/)
  * [make](https://www.gnu.org/software/make/)
  
## Running
  * `make` creates os-image 
  * `make run` builds os-image and runs with qemu
    * To change [qemu display options](https://www.mankier.com/1/qemu) edit this [Makefile line](https://gitlab.com/avinashpai/PaiOS/-/blob/main/Makefile#L17)
  * `make clean` removes \*.o,  \*.bin, etc.
