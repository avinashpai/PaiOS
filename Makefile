CC=i686-elf-gcc
CFLAGS=-ffreestanding -nostdlib -Wall -Wextra -Werror

AS=nasm

LD=i686-elf-ld
LDFLAGS=-nostdlib

C_SRCS = $(wildcard kernel/*.c drivers/*.c drivers/*/*.c utils/*.c)
HEADERS = $(wildcard kernel/*h drivers/*.h drivers/*/*.h utils/*.h)

OBJ = ${C_SRCS:.c=.o}

all: os-image

run: all
	qemu-system-i386 os-image

os-image: boot/boot.bin kernel.bin
	cat $^ > os-image

kernel.bin: kernel.elf
	objcopy kernel.elf -O binary kernel.bin

kernel.elf: kernel/kernel_entry.o ${OBJ}
	$(LD) $(LDFLAGS) -T ld/linker.ld -o $@ $^

%.o : %.c ${HEADERS}
	$(CC) $(CFLAGS) -c $< -o $@

kernel/kernel_entry.o : kernel/kernel_entry.asm
	$(AS) $< -felf -o $@

boot/boot.bin : boot/boot.asm
	$(AS) $< -fbin -I "./boot" -o $@

clean:
	rm -rf *.bin *.dis *.o *.elf os-image
	rm -rf kernel/*.o boot/*.bin drivers/*.o
